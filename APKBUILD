pkgname=nginx-mod-brotli
subpackages="nginx-mod-http-brotli-filter:_module nginx-mod-http-brotli-static:_module"
pkgver=0.1.2
pkgrel=0
pkgdesc="Brotli module for Nginx"
url="https://github.com/google/ngx_brotli"
arch="all"
license="BSD"
# Same as nginx package
nginxver=1.16.1
makedepends="
	gd-dev
	geoip-dev
	libmaxminddb-dev
	libxml2-dev
	libxslt-dev
	linux-headers
	openssl-dev
	paxmark
	pcre-dev
	perl-dev
	pkgconf
	zlib-dev
  brotli-dev
	"
source="
  https://nginx.org/download/nginx-$nginxver.tar.gz
  ngx_brotli-$pkgver.tar.gz::https://github.com/google/ngx_brotli/archive/master.tar.gz
  "
_modules_dir="usr/lib/nginx/modules"
_nginx_mod_http_brotli_filter_depends="nginx brotli"
_nginx_mod_http_brotli_static_depends="nginx"

build () {
  cd "$srcdir/nginx-$nginxver"

  # Tienen que ser las mismas que
  # https://git.alpinelinux.org/aports/plain/main/nginx/APKBUILD?h=3.9-stable
	export LUAJIT_LIB="$(pkgconf --variable=libdir luajit)"
	export LUAJIT_INC="$(pkgconf --variable=includedir luajit)"
	./configure \
		--prefix=/var/lib/nginx \
		--sbin-path=/usr/sbin/nginx \
		--modules-path=/$_modules_dir \
		--conf-path=/etc/nginx/nginx.conf \
		--pid-path=/run/nginx/nginx.pid \
		--lock-path=/run/nginx/nginx.lock \
		--http-client-body-temp-path=/var/tmp/nginx/client_body \
		--http-proxy-temp-path=/var/tmp/nginx/proxy \
		--http-fastcgi-temp-path=/var/tmp/nginx/fastcgi \
		--http-uwsgi-temp-path=/var/tmp/nginx/uwsgi \
		--http-scgi-temp-path=/var/tmp/nginx/scgi \
		--with-perl_modules_path=/usr/lib/perl5/vendor_perl \
		\
		--user=nginx \
		--group=nginx \
		--with-threads \
		--with-file-aio \
		\
		--with-http_ssl_module \
		--with-http_v2_module \
		--with-http_realip_module \
		--with-http_addition_module \
		--with-http_xslt_module=dynamic \
		--with-http_image_filter_module=dynamic \
		--with-http_geoip_module=dynamic \
		--with-http_sub_module \
		--with-http_dav_module \
		--with-http_flv_module \
		--with-http_mp4_module \
		--with-http_gunzip_module \
		--with-http_gzip_static_module \
		--with-http_auth_request_module \
		--with-http_random_index_module \
		--with-http_secure_link_module \
		--with-http_degradation_module \
		--with-http_slice_module \
		--with-http_stub_status_module \
		--with-http_perl_module=dynamic \
		--with-mail=dynamic \
		--with-mail_ssl_module \
		--with-stream=dynamic \
		--with-stream_ssl_module \
		--with-stream_realip_module \
		--with-stream_geoip_module=dynamic \
		--with-stream_ssl_preread_module \
    --add-dynamic-module=../ngx_brotli-master

  local soname; for soname in ${subpackages//-/_}; do
    soname=${soname%:_module}
    make -f objs/Makefile objs/ngx${soname#nginx_mod}_module.so
  done
}

package () {
  depends="$subpackages"
  mkdir -p $pkgdir
}

_module () {
  local name="${subpkgname#nginx-mod-}"
  name="${name//-/_}"
  local sonames="$(eval "echo \$_${name}_so")";
  sonames="${sonames:-"ngx_${name}_module.so"}"

  pkgdesc="$pkgdesc (module $name)"
  depends="$(eval "echo \$_${name}_depends")"

  mkdir -p "$subpkgdir"/$_modules_dir
  mkdir -p "$subpkgdir"/etc/nginx/modules

  cd "$subpkgdir"

  local soname; for soname in $sonames; do
    cp "$srcdir"/nginx-$nginxver/objs/$soname ./$_modules_dir/$soname
    echo "load_module \"modules/$soname\";" > ./etc/nginx/modules/$name.conf
  done
}
